const filesToCache = [
    "/",
    "/manifest.json",
    "https://web2projekt5.onrender.com/public/image/icons/icon.png",
    "https://web2projekt5.onrender.com/views/offline.html",
    "https://web2projekt5.onrender.com/views/404.html"
]

const staticCacheName = 'static-cache'

self.addEventListener('install', event => {
    console.log('Service AAAAAAAworker installing…');
    event.waitUntil(
        caches.open(staticCacheName).then((cache) => {
            return cache.addAll(filesToCache)
                .catch((error) => {
                    console.error('Cache.addAll error:', error);
                });
        })
    );
});

self.addEventListener('activate', event => {
    console.log('Service worker installing…');
    const cacheWhiteList = [staticCacheName]
    event.waitUntil(
        caches.keys().then((cachesNames) =>{
            return Promise.all(
                cachesNames.map((cacheName) => {
                    if(cacheWhiteList.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            )
        })
    )
});

self.addEventListener('fetch', (event) => {
    console.log("fetch");

    const request = event.request;

    // Skip caching if the request scheme is unsupported (e.g., chrome-extension)
    if (request.url.startsWith('chrome-extension://')) {
        return;
    }

    event.respondWith(
        caches.match(request).then((response) => {
            if (response) {
                return response;
            }

            return fetch(request).then((response) => {
                if (response.status === 404) {
                    return caches.match('/public/404.html');
                }

                // Skip caching if the request scheme is unsupported
                if (request.url.startsWith('chrome-extension://')) {
                    return response;
                }

                return caches.open(staticCacheName).then((cache) => {
                    cache.put(request, response.clone());
                    return response;
                });
            }).catch((error) => {
                console.error('Fetch error:', error);
                return caches.match("/public/offline.html");
            });
        })
    );
});

self.addEventListener('push', event => {
    const options = {
      body: event.data.text(),
      icon: '/image/icons/icon.png',
    };
  
    event.waitUntil(
      self.registration.showNotification('Push Notification', options)
    );
  });

let syncSnaps = async function () {
    console.log("***")
}

self.addEventListener("sync", function (event) {
    console.log('888888')
    if (event.tag === "sync-snaps") {
        console.log('(((((')
        event.waitUntil(syncSnaps());
    }
});


 