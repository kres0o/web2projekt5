let videoStream; // Variable to store the camera stream
let isDrawing = false;
let lastX = 0;
let lastY = 0;
let drawingPoints = [];
let isMouseDown = false;
let penWidth = 3; // Default pen width
let penColor = '#000000'; // Default pen color

// Get the video and canvas elements
var video = document.getElementById('camera-feed');
var canvas = document.getElementById('captured-image');
var context = canvas.getContext('2d');

// Function to start the camera
function startCamera() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true })
            .then(function (stream) {
                video.srcObject = stream;
                videoStream = stream; // Store the stream for later use
            })
            .catch(function (error) {
                console.error('Error accessing camera:', error);
                alert('Camera access is not available in this browser.');
            });
    } else {
        console.error('getUserMedia is not supported in this browser');
        alert('Camera access is not available in this browser.');
    }
}

// Function to stop the camera
function stopCamera() {
    if (videoStream) {
        // Stop tracks to release the camera
        videoStream.getTracks().forEach(track => track.stop());
        // Clear the source object of the video element
        video.srcObject = null;
    }
}

// Function to take a picture
function takePicture() {
    // Draw the current frame from the video onto the canvas
    context.drawImage(video, 0, 0, canvas.width, canvas.height);
    // Optionally, you can save the image or perform further processing
    var imageData = canvas.toDataURL('image/png');
    console.log('Captured Image Data:', imageData);
}

// Function to start drawing
function startDrawing() {
    isDrawing = true;
    // Show drawing controls
    document.getElementById('drawing-controls').style.display = 'block';
    // Add event listeners for mouse actions
    canvas.addEventListener('mousedown', handleMouseDown);
    canvas.addEventListener('mousemove', handleMouseMove);
    canvas.addEventListener('mouseup', handleMouseUp);
    // Add a class for drawing
    canvas.classList.add('drawing');
}

// Function to stop drawing
function stopDrawing() {
    isDrawing = false;
    // Hide drawing controls
    document.getElementById('drawing-controls').style.display = 'none';
    // Remove event listeners for mouse actions
    canvas.removeEventListener('mousedown', handleMouseDown);
    canvas.removeEventListener('mousemove', handleMouseMove);
    canvas.removeEventListener('mouseup', handleMouseUp);
    // Remove the class for drawing
    canvas.classList.remove('drawing');
}

// Function to update pen width
function updatePenWidth() {
    penWidth = parseInt(document.getElementById('pen-width').value);
}

// Function to update pen color
function updatePenColor() {
    penColor = document.getElementById('pen-color').value;
}

// Function to handle mouse down event
function handleMouseDown(event) {
    isMouseDown = true;
    let rect = canvas.getBoundingClientRect();
    lastX = event.clientX - rect.left;
    lastY = event.clientY - rect.top;
    drawingPoints.push({ x: lastX, y: lastY });
}

// Function to handle mouse move event
function handleMouseMove(event) {
    if (!isMouseDown || !isDrawing) return;

    let rect = canvas.getBoundingClientRect();
    let currentX = event.clientX - rect.left;
    let currentY = event.clientY - rect.top;

    drawLine(lastX, lastY, currentX, currentY);

    lastX = currentX;
    lastY = currentY;

    drawingPoints.push({ x: lastX, y: lastY });
}

// Function to handle mouse up event
function handleMouseUp() {
    isMouseDown = false;
}

// Function to draw a line with specified pen properties
function drawLine(startX, startY, endX, endY) {
    context.beginPath();
    context.moveTo(startX, startY);
    context.lineTo(endX, endY);
    context.lineWidth = penWidth;
    context.strokeStyle = penColor;
    context.stroke();
    context.closePath();
}

// Function to save the image
function saveImage() {
    // Get the data URL of the canvas and trigger a download
    var link = document.createElement('a');
    link.href = canvas.toDataURL('image/png');
    link.download = 'drawing.png';
    link.click();

    const notificationOptions = {
        body: 'SAVE IMAGE!!!',
        icon: 'https://web2projekt5.onrender.com/public/image/icons/icon.png', // Replace with the path to your notification icon
    };
    new Notification('Notification', notificationOptions);
}

async function setupPushSubscription() {
    try {
        let reg = await navigator.serviceWorker.ready;
        let sub = await reg.pushManager.getSubscription();
        if (sub === null) {
            // Use your server's public VAPID key
            const publicKey = 'BDUykEzqg3KYbjAI_G3LC77lb9tWTYL2i5oHkZ0QPz9MZd9NP7jiicOp2-W5fsSkNYiNi6AHQcGzDfd8_IsxfpY';

            // Subscribe to push notifications
            sub = await reg.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array(publicKey),
            });

            // Save the subscription to the server
            await fetch('/api/saveSubscription', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                },
                body: JSON.stringify({ sub }),
            });

            alert('Yay, subscription generated and saved:\n' + JSON.stringify(sub));
        } else {
            alert('You are already subscribed:\n' + JSON.stringify(sub));
        }
    } catch (error) {
        console.log(error);
    }
}

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }

    return outputArray;
}

// Function to manually send a notification with message "Poslano"
function sendNotifications() {
    const notificationOptions = {
        body: 'Notification works!!!',
        icon: 'https://web2projekt5.onrender.com/public/image/icons/icon.png', // Replace with the path to your notification icon
    };
    new Notification('Notification', notificationOptions);
}

document.getElementById('btnDarkMode').addEventListener('click', async function () {
    if ("serviceWorker" in navigator && "SyncManager" in window) {
        fetch(canvas.toDataURL())
        .then((res) =>  res.blob())
        .then((blob) => {
            return navigator.serviceWorker.ready
        }).then((swRegistration) => {
                console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!')
                return swRegistration.sync.register("sync-snaps");
            })
            .then(() => {
                console.log("Queued for sync");
            })
            .catch((err) => {
                console.log(err);
            });
    } else {
        alert("TODO - vaš preglednik ne podržava bckg sync...");
    }
});