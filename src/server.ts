import express from 'express';
import { Request, Response } from 'express';
import https from 'https';
import dotenv from 'dotenv';
import fs from 'fs';
import bodyParser from 'body-parser';
import webPush from 'web-push';

dotenv.config()

const publicVapidKey = 'BDUykEzqg3KYbjAI_G3LC77lb9tWTYL2i5oHkZ0QPz9MZd9NP7jiicOp2-W5fsSkNYiNi6AHQcGzDfd8_IsxfpY';
const privateVapidKey = 'eVxc4IdmPaJuP2WqLWv8uKkLVohncC7NcgFsOwQ9_no';

webPush.setVapidDetails(
  'mailto:kresooreskovic04@gmail.com',
  publicVapidKey,
  privateVapidKey
);

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;
var boja = '#000000';

const app = express();

app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'))
app.use(express.static(__dirname + '/views'))
app.use(express.urlencoded({ extended: true}))
app.use(express.json());
app.use(ignoreFavicon);

function ignoreFavicon(req: any, res: any, next:any) {
  if (req.originalUrl.includes('favicon.ico')) {
    res.status(204).end()
  }
  next();
}

app.get('/', (req: Request, res: Response) => {
    res.sendFile(__dirname + '/views/index.html')
});

app.post('/api/saveSubscription', (req: Request, res: Response) => {
  const subscription = req.body.sub;

  res.json({ success: true });
});

if (externalUrl) {
  const hostname = '0.0.0.0'; //ne 127.0.0.1
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from
  outside on ${externalUrl}`);
  });
} else {
  https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
  console.log(`Server running at https://localhost:${port}/`);
  });
}